# Turner Code Challenge

## Description
This is an Restful API for the backend portion of the challenge.

##### Routing         : Express
##### ODM Database    : Mongoose
##### Authentication  : Passport, JWT

## Installation

#### Donwload Code | Clone the Repo

```
git clone https://kosayande@bitbucket.org/kosayande/turner-code-challenge.git
```

#### Install Node Modules
```
npm install
```

#### Create .env File
You will find a example.env file in the home directory. Paste the contents of that into a file named .env in the same directory. 
I removed the Movie DB API key
#### Run Project
```
npm start
```

#### For Routes that require athentication, the Bearer acessToken returned in /login needs to be add to the Authorization Header

