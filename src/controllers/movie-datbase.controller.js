'use strict'

const { movieDbService } = require('../services')

const { movieSearch, getMovieDetails, rateMovie } = movieDbService

const search = async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  const query = req.query.query
  try {
    const response = await movieSearch(query)
    res.status(201).send(response)
    next()
  } catch (error) {
    res.sendStatus(500) && next(error)
  }
}

const get = async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  const movieId = req.params.movieId
  try {
    const response = await getMovieDetails(movieId)
    res.status(200).send(response)
    next()
  } catch (error) {
    res.sendStatus(500) && next(error)
  }
}

const createRating = async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  try {
    const response = await rateMovie(req.user, req.body)
    res.status(201).send(response)
    next()
  } catch (error) {
    res.sendStatus(500) && next(error)
  }
}

module.exports = {
  search,
  get,
  createRating
}
