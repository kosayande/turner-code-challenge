const user = require('./user.controller')
const movieDb = require('./movie-datbase.controller')

module.exports = {
  user,
  movieDb
}
