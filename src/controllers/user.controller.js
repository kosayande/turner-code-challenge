'use strict'

const { userService } = require('../services')

const { authUser, createUser } = userService

const login = async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  const { email, password } = req.body
  try {
    const user = await authUser(email, password)
    // other service call (or same service, different function can go here)
    // i.e. - await generateBlogpostPreview()
    res.status(201).send({ accessToken: user.getJWT(),
      guestSessionId: user.movieDbSessionId,
      guestSessionExpirationTime: user.movieDbSessionIdExpirationDate })
    next()
  } catch (error) {
    res.sendStatus(500) && next(error)
  }
}

const get = async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  const user = req.user
  res.status(200).send(user)
  next()
}

const create = async (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  try {
    const newUser = await createUser(req.body)
    res.status(201).send({ accessToken: newUser.getJWT(),
      guestSessionId: newUser.movieDbSessionId,
      guestSessionExpirationTime: newUser.movieDbSessionIdExpirationDate })
    next()
  } catch (error) {
    console.log(error)
    res.sendStatus(500) && next(error)
  }
}

module.exports = {
  login,
  get,
  create
}
