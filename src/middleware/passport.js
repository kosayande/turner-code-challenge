'use strict'

const { ExtractJwt, Strategy } = require('passport-jwt')
const { User } = require('../models')
const CONFIG = require('../config/config')

module.exports = function (passport) {
  const opts = {}
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
  opts.secretOrKey = CONFIG.jwtEncryption

  passport.use(new Strategy(opts, async function (jwtPayload, done) {
    try {
      const user = await User.findById(jwtPayload.user_id)
      if (user) {
        return done(null, user)
      } else {
        return done(null, false)
      }
    } catch (error) {
      console.log(error)
      return done(error, false)
    }
  }))
}
