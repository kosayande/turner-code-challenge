'use strict'

const axios = require('axios')
const qs = require('qs')

const MovieDatabaseClient = axios.create({
  baseURL: 'http://api.themoviedb.org/3',
  params: {
    api_key: process.env.movieAPIKey
  },
  paramsSerializer: function (params) {
    return qs.stringify(params, { arrayFormat: 'repeat' })
  }
})

module.exports = {
  MovieDatabaseClient
}
