'use strict'

const express = require('express')
const passport = require('passport')
const swaggerUi = require('swagger-ui-express')
// const swaggerDocument = require('./swagger.json');

const { user, movieDb } = require('../controllers')

require('./../middleware/passport')(passport)

const router = express.Router()

// router.use('/', swaggerUi.serve);
// router.get('/', swaggerUi.setup(swaggerDocument));
router.post('/authentication/login', user.login)
router.post('/authentication/sign-up', user.create)
router.get('/users', passport.authenticate('jwt', { session: false }), user.get)
router.get('/search/movie', passport.authenticate('jwt', { session: false }), movieDb.search)
router.get('/movie/:movieId', passport.authenticate('jwt', { session: false }), movieDb.get)
router.post('/movie/rating', passport.authenticate('jwt', { session: false }), movieDb.createRating)

module.exports = router
