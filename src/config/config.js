require('dotenv').config({ path: '.env' })

const CONFIG = {}

CONFIG.movieAPIKey = process.env.movieAPIKey
CONFIG.databaseUrl = process.env.databaseUrl
CONFIG.jwtExpirationTime = process.env.jwtExpirationTime
CONFIG.jwtEncryption = process.env.jwtEncryption

module.exports = CONFIG
