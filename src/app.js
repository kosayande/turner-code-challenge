'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./routes')
const app = express()
const displayRoutes = require('express-routemap');
const CONFIG = require('./config/config')
const models = require('./models')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/api', routes)

app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.sendStatus(err.status || 500);
  // res.render('error');
  next()
});

app.listen(3000, () => {
  console.log('Api listening on port 3000!')
  displayRoutes(app);

})
