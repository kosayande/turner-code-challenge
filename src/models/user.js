'use strict'

const mongoose = require('mongoose')
const validate = require('mongoose-validator')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const CONFIG = require('../config/config')

const userSchema = mongoose.Schema({
  email: { type: String,
    lowercase: true,
    trim: true,
    index: true,
    unique: true,
    sparse: true,
    validate: [validate({
      validator: 'isEmail',
      message: 'Not a valid email.'
    })] },
  password: String,
  movieDbSessionId: String,
  movieDbSessionIdExpirationDate: Date
})

userSchema.pre('save', async function (next) {
  if (this.isModified('password') || this.isNew) {
    let err, salt, hash
    salt = await bcrypt.genSalt(10)
    hash = await bcrypt.hash(this.password, salt)
    this.password = hash
  } else {
    return next()
  }
})

userSchema.methods.comparePassword = async function (password) {
  const isMatch = await bcrypt.compare(password, this.password)
  if (isMatch) {
    return this
  }
}

userSchema.methods.getJWT = function () {
  const expirationTime = parseInt(CONFIG.jwtExpirationTime)
  return 'Bearer ' + jwt.sign({ user_id: this._id }, CONFIG.jwtEncryption, { expiresIn: expirationTime })
}

const User = mongoose.model('User', userSchema)
module.exports = User
