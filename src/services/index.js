'use strict'

const movieDbService = require('./movie-databse.service')
const userService = require('./user.service')

module.exports = {
  movieDbService,
  userService
}
