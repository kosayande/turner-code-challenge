'use strict'

const { MovieDatabaseClient } = require('../utils/index')

const createNewGuestSession = async () => {
  try {
    const response = await MovieDatabaseClient.get('/authentication/guest_session/new')
    return response.data
  } catch (error) {
    console.log(error)
  }
}

const movieSearch = async (query) => {
  try {
    const response = await MovieDatabaseClient.get('/search/movie', {
      params: {
        query: query
      }
    })
    return response.data
  } catch (error) {
    console.log(error)
  }
}

const getMovieDetails = async (movieId) => {
  try {
    const response = await MovieDatabaseClient.get(`/movie/${movieId}`)
    return response.data
  } catch (error) {
    console.log(error)
  }
}

const rateMovie = async (user, ratingInfo) => {
  try {
    const response = await MovieDatabaseClient.post(`/movie/${ratingInfo.movieId}/rating`, {
      params: {
        guest_session_id: user.movieDbSessionId
      },
      data: {
        value: ratingInfo.ratingValue
      }
    })
    return response.data
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  createNewGuestSession,
  movieSearch,
  getMovieDetails,
  rateMovie
}
