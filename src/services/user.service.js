'use strict'

const { User } = require('../models')
const movieDbService = require('./movie-databse.service')

const createUser = async (user) => {
  try {
    const newUser = new User(user)
    const guestSession = await movieDbService.createNewGuestSession()
    newUser.movieDbSessionId = guestSession.guest_session_id
    newUser.movieDbSessionIdExpirationDate = guestSession.expires_at
    return await newUser.save()
  } catch (error) {
    console.log(error)
  }
}

const authUser = async (email, password) => {
  try {
    const user = await User.findOne({ email: email })
    return await user.comparePassword(password)
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  createUser,
  authUser
}
